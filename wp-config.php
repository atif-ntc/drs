<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'drs' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'D&op)aOU4i~#Rq`?#>{&[I[L!CM[koYZKB`@k95qt!JKlp$sM^t_*P!S(<AlX5c4' );
define( 'SECURE_AUTH_KEY',  'D-JbX#ooY+GH;iM+z6RmHEK(u17V9qIz)<ddpObf-Bu;!t9:l>+4Vkd*)X:>FgQw' );
define( 'LOGGED_IN_KEY',    '5EJA N!(}&5-/=8CqS ,DW&/:}|*d%uluk-j3~E9Bj<HD-;:HydT]44 ?$I^/G.f' );
define( 'NONCE_KEY',        'g#kowbA_nbdxpB?LkB,kGq++<&=K&`OK}c 96i1SIX#2mb7DYEg>ZkX r!HPV?14' );
define( 'AUTH_SALT',        '+-oH*#;u`N-BJHD%ah%`dJg(YP@}h9*n2]yxqT^[a%kbK,pLz]S!a@CUW0AbU+gV' );
define( 'SECURE_AUTH_SALT', ']b4h __h14pyY<Z-u<x,.?_J;{SS*3DR4JAo|JWnOwzBW@Uv_-#ze;U1nsxhf3G$' );
define( 'LOGGED_IN_SALT',   'It|D^,NZ%OH;jh$X:ysy]?b1j`4_TL7%<@Z3}sFIpgEur}<o.pRC7O0fL2kRIDmj' );
define( 'NONCE_SALT',       'WU9z@_4i6p!U+@1A?cwqBz2^J0$U)ev~3hz&xV`Q_!KpS ~82!`bK8#6(~6Vjwh ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
